<!-- Ideal place to connect php files -->
<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="widtd=device-width initial-scale=1">

		<title>s02: Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>

		<h1>Repitition Control Structures</h1>

		<h2>While Loop</h2>
		<?php whileLoop(); ?>

		<br/>

		<h2>Do-While Loop</h2>
		<?php doWhileLoop(); ?>

		<br/>

		<h2>For Loop</h2>
		<?php forLoop(); ?>

		<br/>

		<h2>Continue and Break Statement</h2>
		<?php modifiedForLoop(); ?>

		<br/>

		<h2>Associative Arrays</h2>
		<?php echo $gradePeriods['secondGrading'] ?>


		<ul>
			<?php foreach ($gradePeriods as $period => $grade) { // <?= is the shortcut for <?php echo ?>
				<li>Grade in <?= $period ?> is <?= $grade ?></li>
			<?php } ?>
		</ul>

		<h2>Two-Dimensional Arrays</h2>
		<?php echo $heroes[2][1] ?>

		<ul>
			
			<?php 

				foreach($heroes as $team) {
					foreach($team as $member) { ?>
						<li><?php echo $member ?></li>
					<?php }
				}
			?>

		</ul>

		<h2>Sorting Arrays</h2>
		<pre>
			<?php print_r($sortedBrands); ?>
		</pre>

		<h2>Reverse Sorting Arrays</h2>
		<pre>
			<?php print_r($reverseSortedBrands); ?>
		</pre>

		<h2>Array Mutations (Append)</h2>

		<!-- Adds item to the end of the array -->
		<h3>Push</h3>
		<?php array_push($computerBrands, 'Apple'); ?>
		<pre>
			<?php print_r($computerBrands); ?>
		</pre>

		<!-- Add items to the beginning of the array -->
		<h3>Unshift</h3>
		<?php array_unshift($computerBrands, 'Dell'); ?>
		<pre>
			<?php print_r($computerBrands); ?>
		</pre>

		<h2>Remove</h2>

		<!-- Remove item to the end of the array -->
		<h3>Pop</h3>
		<?php array_pop($computerBrands); ?>
		<pre>
			<?php print_r($computerBrands); ?>
		</pre>

		<!-- Remove items to the beginning of the array -->
		<h3>Shift</h3>
		<?php array_shift($computerBrands); ?>
		<pre>
			<?php print_r($computerBrands); ?>
		</pre>
		
		<!-- To check the length of the array -->
		<h2>Count</h2>
		<?php echo count($computerBrands); ?>

		<h2>Search</h2>
		<p>
			<?php echo searchBrand($computerBrands, 'Asus'); ?>
		</p>

		<h2>Reverse</h2>
		<pre>
			<?php print_r($reverseGradePeriods); ?>
		</pre>

		<!-- Explore More Here: https://sabe.io/classes/php/arrays -->
	</body>
</html>